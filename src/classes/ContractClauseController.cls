public class ContractClauseController {
    public String RecordToShow {get;set;}
    public String RecordToDelete {get;set;}
    public Contract_Clause_Assignment__c junctionObj {get;set;}
    public ContractClauseController() {}
    public ContractClauseController(ApexPages.StandardController controller) {
        junctionObj = new Contract_Clause_Assignment__c();
    }
    public Contract getContract() {
        Contract result = Database.query(
            'SELECT Id, Account.Name, ContractNumber, Status, StartDate, EndDate, ContractTerm ' +
            'FROM Contract ' +
            'WHERE Id = \'' + RecordToShow + '\''
        );
        return result;
	}  
    public List<Contract_Clause_Assignment__c> getClauseAssignments() {
        List<Contract_Clause_Assignment__c> assignmentResults = Database.query(
    		'SELECT Name, Clause__c, Contract__c FROM Contract_Clause_Assignment__c ' + 
            'WHERE Contract__r.Id = \'' + RecordToShow + '\''
    	);
        return assignmentResults;
    }   
    public void deleteClauseAssignment() {
        RecordToDelete = apexpages.currentPage().getParameters().get('clauseToDelete');
        Contract_Clause_Assignment__c cl = Database.query('SELECT Name FROM Contract_Clause_Assignment__c ' +
        	'WHERE Id = \'' + RecordToDelete + '\''
        );
        delete cl;
    }  
    public PageReference newClauseAssignment() {
        PageReference prf = new PageReference('/apex/ClauseSelector?ct=' + RecordToShow);
        return prf;
    }  
    public PageReference save() {
        junctionObj.Contract__c = RecordToShow;
        upsert junctionObj;
        PageReference prf = new PageReference('/apex/ContractClauseAssignment?ct=' + RecordToShow);
     	junctionObj = new Contract_Clause_Assignment__c();
        return prf;
    } 
    public PageReference cancel() {
        PageReference prf = new PageReference('/apex/ContractClauseAssignment?ct=' + RecordToShow);
        return prf;
    } 
    public void setId() {
        RecordToShow = apexpages.currentPage().getParameters().get('ct');
    }
}