@isTest
public class ContractClauseControllerTest {
    Static testMethod void myUnitTest(){
        test.startTest();
        Account acct = new Account(Name='FirstNameTest');
        insert acct;
        Contract ct = new Contract();
        ct.Status='Draft';
        ct.AccountId= acct.Id;
        insert ct;
        Contract_Clauses__c cl = new Contract_Clauses__c();
        cl.Name ='ClauseNameTest';
        cl.Type__c='Other';
        insert cl;
        Contract_Clause_Assignment__c ccA = new Contract_Clause_Assignment__c();
        ccA.Name='ClauseAssignmentNameTest';
        ccA.Clause__c=cl.Id;
        ApexPages.StandardController sc = new ApexPages.StandardController(ct);
        ContractClauseController cccObj = new ContractClauseController(sc);
        ContractClauseController cccObjEmptyConst = new ContractClauseController();
        cccObj.junctionObj = ccA;
        ApexPages.currentPage().getParameters().put('ct', ct.Id);
        cccObj.setId();
        cccObj.getContract();
        cccObj.getClauseAssignments();
        cccObj.newClauseAssignment();
        cccObj.save();
        ApexPages.currentPage().getParameters().put('clauseToDelete', ccA.Id);
        cccObj.deleteClauseAssignment();
        cccObj.cancel();
        test.stopTest();  
    }
}